#/usr/bin/python3
from collections import defaultdict
import re
import boto3
from prettytable  import PrettyTable
"""
A tool for retrieving basic information from the running EC2 instances.
"""
class Checkvolumes:

    def __init__(self):
        """"class constructor """

        self.HEADER = '\033[95m'
        self.OKBLUE = '\033[94m'
        self.OKGREEN = '\033[92m'
        self.WARNING = '\033[93m'
        self.FAIL = '\033[91m'
        self.ENDC = '\033[0m'
        self.BOLD = '\033[1m'
        self.UNDERLINE = '\033[4m'

    def volumes_info(self):

        # Connect to EC2
        ec2 = boto3.resource('ec2')

        # Get information for all running instances
        running_instances = ec2.instances.filter(Filters=[{
            'Name': 'instance-state-name',
            'Values': ['running','stopped']}])

        ec2info = defaultdict()
        Numlist =[]
        for instance in running_instances:
            for tag in instance.tags:
                if 'Name'in tag['Key']:
                        name = tag['Value']

            volumes = instance.volumes.all()
            for v in volumes:
                volume_id = v.id
                encrypted_status = v.encrypted

            ec2info[instance.id] = {
                        'Name': name,
                        'Type': instance.instance_type,
                        'State': instance.state['Name'],
                        'Volume': volume_id,
                        'encrypted': encrypted_status,
                        'Launch Time': instance.launch_time
                        }
        return ec2info

    def show(self,ec2info):
        '''Show with PrettyTable results'''
        x = PrettyTable()
        x.field_names = ["id", "Name", "volume", "encrypted_status"]
        for instance_id, info in ec2info.items():
            #print(info)
            if info['encrypted'] == True:
                id = self.OKGREEN + str(instance_id)
            else:
                id = self.FAIL + str(instance_id)

            x.add_row([id,info['Name'],info['Volume'],info['encrypted']])
        print(x)

a = Checkvolumes()
infos = a.volumes_info()
a.show(infos)

    # attributes = ['Name', 'Type', 'State', 'Volume', 'encrypted', 'Launch Time']
    # for instance_id, instance in ec2info.items():
    #     for key in attributes:
    #         print("{0}: {1}".format(key, instance[key]))
    #     print("------")
